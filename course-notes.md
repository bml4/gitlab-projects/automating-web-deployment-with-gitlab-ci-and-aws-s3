# Introduction to Automating web deployment with GItlab CI and AWS S3 - Course Notes

## Unit 1 - Introduction to GitLab

### Lesson 1 - Welcome

- This course is for people new to DevOps who want to use GitLab to build, test and deploy their software.
- You will get hands-on experience building pipelines with GitLab CI and deploying software to AWS S3 bucket as a static website.
- You don't need to install anything on your system, you will be able to do it just on your browser.

### Lesson 2 - GitLab architecture

#### GitLab CI: Your Development Orchestrator

GitLab CI empowers you to automate your software development pipeline, from building and testing to deployment. Let's explore its key components:

**The Players:**

1. **GitLab Server (Coordinator)**: The mastermind behind the scenes, managing pipeline execution, assigning tasks to Runners, and collecting results. Think of it as the conductor of your development orchestra.
2. **GitLab Runner (The Builder):** This lightweight program tirelessly executes your specified tasks. Imagine it as the dedicated musician, following instructions and delivering results.

**The Performance:**

1. **Job Dispatch:** When a job needs to be played, the Coordinator finds an available Runner to perform it.
2. **Docker Magic:** Runners often utilize Docker containers for isolated and efficient execution. Think of them as temporary practice rooms with specific instrument setups.
3. **Code Retrieval:** The Runner fetches the code from your Git repository, ensuring everyone's playing the same sheet music.
4. **Scripted Actions:** The "script" section in your job YAML file defines the commands the Runner executes within the container. This is where the music comes to life!
5. **Reporting Back:** Once the job finishes, the Runner transmits the results and feedback to the Coordinator for evaluation.
6. **Wrapping Up:** The Runner concludes its performance, ready for the next piece.
  
#### Beyond the GitLab CI Basics & GitLab Benefits:

While above explanation covers the core of GitLab CI, there's more to discover! Runners can have customized configurations for specific tasks, like specialized tools for building mobile apps or testing on different operating systems. Additionally, GitLab CI offers advanced features like caching and artifacts to boost performance and save time.

**But why does this matter to you?** By mastering GitLab CI, you unlock a powerful set of benefits:

1. **Unleash your creativity:** Automate repetitive tasks like building, testing, and deploying, freeing your mind for innovation and problem-solving.
2. **Guarantee quality:** Implement consistent testing and code review practices throughout your development process, ensuring high-quality software.
3. **Speed up your workflow:** Streamline your development pipeline, reducing bottlenecks and getting your projects to market faster.

**Beyond GitLab CI, GitLab itself offers even more advantages:**

1. **Version control:** Keep track of every change made to your project, allowing you to revert to previous versions or collaborate effectively with others.
2. **Secure file storage:** Store your code and assets safely and reliably, with access control and disaster recovery measures in place.
3. **Seamless collaboration:** Work together on projects with ease, discuss code changes, and manage tasks efficiently, even with geographically dispersed teams.

By leveraging GitLab and GitLab CI, you can achieve greater efficiency, maintain quality, and empower your team to deliver successful projects faster. Dive deeper and explore the full potential of these powerful tools!

### Lesson 3 - Your first GitLab project

#### Setup SSH Key

- Install Git
  - [Setup GitBash in Windows Platform](https://www.oracle.com/webfolder/technetwork/tutorials/ocis/ocis_fundamental/gitbash-inst.pdf)
  - [Setup Git on linux](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

- Configure Git
  
```bash
git config --global user.name "First name Last name"
git config --global user.email “you@example.com”
```

You will get an error but we will fix it now

- Generate an SSH Key

We will be using a tool called **ssh-keygen**.

Run Command

```bash
ssh-keygen -t rsa -b 2048
```

First, you will be asked about the location where the keys should be stored. By default, your user folder will contain a folder called .ssh.
    - copy the file name and paste it after `:` ends the path with `git_lab`
    - Enter
    - Run commands below one after the other
  
```bash
cd ~/.ssh
ls

```

![ssh-keygen](docs/image/ssh-keygen.png)

    - git_lab — this is your PRIVATE key. This is your secret. DO NOT SHARE this with anyone else.
    - git_lab.pub — this is your PUBLIC key. This contains no secrets. You can share it with others.

- Add your SSH key to Gitlab
  - copy the content of your public key
  
  ```bash
  cat git_lab.pub
  ```

  - Go to your profile settings by clicking your image that represent your profile.
  - Select SSH Keys > add new key
  - Paste the public key inside the `key` box > Add key

- Clone a Gitlab repository
Log in to your GitLab account and go to the repository you want to clone

![project-clone](docs/image/project-clone.png)

Click on the Clone button and the address under Clone with SSH

Run the command

```bash
git clone (PASTE HERE YOUR ADDRESS)
```

- We will be using [GitLab.com](https://gitlab.com/) in this course
- Create a free GitLab.com account
- Create a project > Create blank project > Project name "my first pipeline" > Private > create project
- change the theme: Profile > Preferences > Syntax highlighting theme > Monokai
  - help to have dark theme when writing code
- Enable *Render whitespace characters in the Web IDE*
  - This will show us any whitespace character whenever editing files
- Save changes
- Go back to Gitlab by clicking Gitlab image
- click on projects > "YOUR-PROJECT"
- Use WEB IDE to create pipeline definition file
  - click on the `+` sign beside your project name > click `New file`
  - name `.gitlab-ci.yml` > Commit changes > edit > Open in Web IDE

   GitLab CI pipelines are defined in a file called .gitlab-ci.yml.
   If its not exert this name, GitLab will not recongnized the pipeline.
   Write here configurations to create pipelines in GitLab.
  
```bash
test:
   script: echo "Hello world"
```

- Commit the changes
- Down below by the right and side click on `Go to Project`
  - You will see that the pipeline passed
  - Go to Build > Pipelines > click on `Passed` job to see job logs > click on `test`
    - It pull docker image *ruby* by default, if image is not explicitly specify in a job

### Lesson 4 - Your first pipeline

**What is pipeline?**
This is a step by step way of building software. 
There are series of steps involve in building a software:

- Code
- Build
- Test
- Packaging
- Delivery/Deployment
- Operation

A **job** is a set of command we want to run/execute. We define the GitLab CI pipeline using YAML.

**YAML** is a way to represent key-value pairs.
A pipeline is composed of a series of jobs organized in stages.

- `mkdir build` - creates a new folder called `build`
- `touch file.txt` - creates a new file called `file.txt`
- `>>` is called a redirection operator and appends the output from a previous command to a file
- `cat` can be used for displaying the contents of a file
- Use **Linux Alpine** for this job because it is a very lightweight distribution
- If no stage is defined in the job config, the *test* stage will be assigned


#### 📚 Resources

- [Pipeline after this lecture](docs/pipeline-configs/lesson-1-03-.gitlab-ci.yml)
- [Alpine Linux](https://www.alpinelinux.org/)

### Lesson 5 - Pipeline common errors

- here are some common mistakes that lead to errors in the jobs:
  - no space after `-`, like `-echo "Foo"`
  - bad indentation
  - check if you have named the pipeline exactly `.gitlab-ci.yml`
  - forgetting to add column `:` after stages: , build: , script:
  - root config contains unknown keys: <some job name>

```yaml
deploy review:
  stage: deploy review
  only:
    - merge requests
  variables:  
    GIT_STRATEGY: none
    script:
    - echo "Deploying on review"
    - sh ./deploy.sh
```

GitLab is not recognizing the configuration deploy reviewas a job. The problem here is that this job configuration is missing the script block. Because the script is not properly indented, it has been defined as a variable.

- (<unknown>): did not find expected key while parsing a block mapping at line 1 column 1
  
  What seems to be the issue here? I can assure you there is no issue on line 1 column 1. The problem is bad indentation. The echo command in the first job is not properly indented. The second job is also not properly indented. Writing yaml file, indentation is very important.

```yaml
stages:
  - build
  - test

build the car:
  stage: build
  script:
  - echo "Building my car"

  test the car:
  stage: test
  script:
  - echo "Testing my car"
```

This is how the configuration should look like:

```yaml
stages:
  - build
  - test

build the car:
  stage: build
  script:
    - echo "Building my car"

test the car:
  stage: test
  script:
    - echo "Testing my car"
```


### Lesson 6 - What is YAML?

- you need to know some YAML basics to write GitLab CI pipelines
- YAML is somewhat similar to JSON or XML
- XML, JSON and YAML, and human-readable data interchange formats
- YAML is being often used for storing configurations

### Lesson 7 - What is a shell?

- we typically run commands such as `echo`, `touch`, `mkdir`, `cat` and so on through a command-line interface or CLI
- to automate the building & releasing of software, we rely on tools that have no UI, so we need to use the CLI

### Lesson 8 - Pipeline stages

- by default, a job will be assigned to the *Test* stage
- if two or more jobs belong to the same stage, they will be executed in parallel
- the `stages:` keyword allows us to define the stages of the pipeline
- the keyword `stage:` allows us to associate a job with a stage

#### 📚 Resources

- [Pipeline after this lecture](docs/pipeline-configs/lesson-1-08-.gitlab-ci.yml)

### Lesson 9 - Why do pipelines fail?

- CLI programs indicate if their execution was successful or not by returning an exit code
- an exit code 0 will indicate that a program has been executed successfully
- any other exit code, which can be from 1 to 255, indicates failure
- if GitLab detects a non-zero exit code, the job execution stops
- **Highly important tip:** reading the job logs from top to bottom is KEY to understanding WHY a job has failed
- ERROR: Job failed: exit code 1
  
    This is a very generic error. You need to look at the console logs from the beginning to the end to understand which commands have been executed and what has happened.

    Understanding your logs is key to understanding your pipeline.

    If something does not work, try breaking down the job into smaller parts to identify the root cause for your error.

    Once you have identified that error, search for that specific error.

### Lesson 10 - Job artifacts

- every job is executed in a separate container, so by default, no files are shared
- to save the build results, we need to define the file(s) or folders as artifacts
- in GitLab, we do this by using the artifacts keyword:

```yaml
build laptop:
    ...
    artifacts:
        paths:
            - build

```

- common error associated with **artifacts and paths keyword**
  
  - config contains unknown keys: path error
  
    The way you specify the build folder as an artifact is by setting the `paths` property. This is a plural as it can specify a list of files or folders which will be published as artifacts. The `artifacts` keyword is also plural.

#### 📚 Resources

- [Pipeline after this lecture](docs/pipeline-configs/lesson-1-10-.gitlab-ci.yml)

### Lesson 11 - Testing the build

- our goal is to automate both the build process and the test process
- currently, we are only testing the content of the file by downloading the job artifacts or by using the `cat` command
- to automate the testing process, we will use the `grep` command
- `grep` allows us to search for a specific string in a file.

```
        - grep "Display" build/computer.txt
```

- tests play a very important role in automation
- you need to "test" the tests, to ensure that they will fail if needed

#### 📚 Resources

- [Pipeline after this lecture](docs/pipeline-configs/lesson-11-.gitlab-ci.yml)

### Lesson 12 - Variables

- we prefer to use variables instead of repeating a string in the pipeline configuration
- variables can be defined in scripts or using the `variables:` keyword
- to reference the variable, we use the dollar sign before it

```yaml
variables:
  BUILD_FILE_NAME: laptop.txt
```

- variables can be defined locally in the job or globally for all jobs
- when using spaces or some special characters, you may need to put the entire value between quotes

### Lesson 13 - What is DevOps

- DevOps is not a standard and does not have an universally agreed definition
- DevOps is not a standard or a tool, but a set of practices
- DevOps uses automation to that help us build successful Products
- DevOps requires a different mindset and works really well with Agile & Scrum

#### 📚 Resources

- [Introduction to Agile & Scrum](https://skl.sh/3LciAkr)

## Unit 2 - Continuous Integration with GitLab CI

### Lesson 1 - Unit overview

- we will start working on a simple website project
- we want to automate any of the manual steps required for integrating the changes of multiple developers
- we will create a pipeline that will build and test the software we are creating

### Lesson 2 - Your first GitLab project

- we will try to automate the build and deployment of a simple website project build with JavaScript using the React framework
- forking allows making a copy of a project
- the key to automation is to be familiar with the CLI tools we plan to use in GitLab CI

#### 📚 Resources

- [Website project (fork this)](https://gitlab.com/gitlab-course-public/freecodecamp-gitlab-ci)

### Lesson 3 - Building the project

- most software projects have a build step where code is compiled or prepared for production-use
- yarn is a Node.js package manager that helps with managing the project by running scripts and installing dependencies
- for a Node.js project, the `node_modules` folder contains all project dependencies
- project dependencies are installed using `yarn install` but are NOT stored in the Git repository
- it is really a bad idea to store project dependencies in the code repository
- image tags that contain `alpine` or `slim` are smaller in size as they use a lightweight Linux distribution

#### 📚 Resources

- [Official Node.js images on Dockerhub](https://hub.docker.com/_/node)
- [Check the current Node.js LTS version](https://nodejs.org/en/)
- [Yarn CLI documentation](https://yarnpkg.com/cli/)
- [Pipeline after this lecture](docs/pipeline-configs/lesson-2-03-.gitlab-ci.yml)

### Lesson 4 - Assignment

- Create a job to verify that the build folder contains a file named index.html
- Create another job that runs the project unit tests using the command yarn test

### Lesson 5 - Assignment solution

#### 📚 Resources

- [My solution to the assignment](docs/pipeline-configs/lesson-2-05-.gitlab-ci.yml)

### Lesson 6 - How do we integrate changes?

- we use Git to keep track of code changes
- we need to ensure we don't break the main pipeline

#### 📚 Resources

- [Git for GitLab](https://www.youtube.com/watch?v=4lxvVj7wlZw)

### Lesson 7 - Merge requests

- we need to ensure that the chances of breaking the main branch are reduced
- here are some project settings for working with Merge Requests that I recommend:
  - Go to Settings > Merge requests
  - Merge method > select *Fast-forward merge*
  - Squash commits when merging > select *Encourage*
  - Merge checks > check *Pipelines must succeed*
- protect the master by allowing changes only through a merge request:
  - Settings > Repository > Branch main > Allowed to push - select *No one*

#### 📚 Resources

- [Pipeline after this lecture](docs/pipeline-configs/lesson-2-07-.gitlab-ci.yml)

### Lesson 8 - Code review

- merge requests are often used to review the work before merging it
- merge requests allow us to document why a specific change was made
- other people on the team can review the changes and share their feedback by commenting
- if you still need to make changes from the merge request, you can open the branch using the Web IDE

#### 📚 Resources

- [Real-world merge request example](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/79236)

### Lesson 9 - Integration tests

- before we ship the final product, we try to test it to see if it works
- testing is done of various levels but high-level tests typically include integration and acceptance tests
- we use cURL to create an HTTP call to the website

#### 📚 Resources

- [Pipeline after this lecture](docs/pipeline-configs/lesson-2-09-.gitlab-ci.yml)

### Lesson 10 - How to structure a pipeline

- our current pipeline structure is just an example, not a rule
- there are a few principles to consider
- principle #1: Fail fast
  - we want to ensure that the most common reasons why a pipeline would fail are detected early
  - put jobs of the same size in the same stage
- principle #2: Job dependencies
  - you need to understand the dependencies between the jobs
  - example: you can't test what was not already built yet
  - if jobs have dependencies between them, they need to be in distinct stages

## Unit 3 - Continuous Deployment with GitLab & AWS

### Lesson 1 - Unit overview

- we will learn about deployments and take our website project and deploy it to the AWS cloud.
- learn about other DevOps practices such as CI/CD

### Lesson 2 - A quick introduction to AWS

- AWS (Amazon Web Services) is a cloud platform provider offering over 200 products & services available in data centers all over the world
- you need an AWS account to continue with the rest of the course

#### 📚 Resources

- [Amazon Web Services](https://aws.amazon.com/)

### Lesson 3 - AWS S3

- the first AWS service that we will use is AWS S3 which stands for simple storage service
- the website is static and requires no computing power or a database
- we will use AWS S3 to store the public files and serve them over HTTP
- AWS S3 files (which AWS calls objects) are stored in buckets
- the name of the bucket needs to be unique
- the AWS console allows us to manually upload files through the web interface

### Lesson 4 - AWS CLI

- to interact with the AWS cloud services, we need to use a CLI tool called AWS CLI
- we will use AWS CLI v2 throughout the course
- when using Docker images in pipelines, I highly recommend specifying a tag or at least a major version (if available)
- if you don't specify a tag, at least log the version of every tool you use, as this can help with debugging later on
- example: `aws --version`

#### 📚 Resources

- [AWS CLI documentation](https://awscli.amazonaws.com/v2/documentation/api/latest/reference/index.html)
- [AWS CLI on Dockerhub](https://hub.docker.com/r/amazon/aws-cli)
- [Pipeline after this lecture](docs/pipeline-configs/lesson-3-04-.gitlab-ci.yml)

### Lesson 5 - Uploading a file to S3

- to upload a file to S3, we will use the copy command `cp`
- `aws s3 cp` allows us to copy a file to and from AWS S3

#### 📚 Resources

- [AWS CLI for S3 documentation](https://awscli.amazonaws.com/v2/documentation/api/latest/reference/s3/index.html)
- [Pipeline after this lecture](docs/pipeline-configs/lesson-3-05-.gitlab-ci.yml)

### Lesson 6 - Masking & protecting variables

- to define a variable, go to *Settings > CI/CD > Variables > Add variable*
- we typically store passwords or other secrets
- a variable has a key and a value
- it is a good practice to write the key in uppercase and to separate any words with underscores
- flags:
  - Protect variable: if enabled, the variable is not available in branches, apart from the default branch (main), which is a protected branch
  - Mask variable: if enabled, the variable value is never displayed in clear text in job logs

#### 📚 Resources

- [Pipeline after this lecture](docs/pipeline-configs/lesson-3-06-.gitlab-ci.yml)

### Lesson 7 - Identity management with AWS IAM

- we don't want to use our username and password to use AWS services from the CLI (I am not even sure if this is even possible!)
- as we only need access to S3, it makes sense to work with an account with limited permissions
- IAM allows us to manage access to the AWS services
- we will create a new user with the following settings:
  - account type: programmatic access
  - permissions: attach existing policy: AmazonS3FullAccess
- the account details will be displayed only once
- go to *Settings > CI/CD > Variables > Add variable* and define the following unprotected variables:
  - AWS_ACCESS_KEY_ID
  - AWS_SECRET_ACCESS_KEY
  - AWS_DEFAULT_REGION
- AWS CLI will look for these variables and use them to authenticate

### Lesson 8 - Uploading multiple files to S3

- using cp to copy individual files can take a lot of space in the pipeline config
- some file names are generated during the build process, and we can't know them in advance
- we will use sync to align the content between the build folder in GitLab and the S3 bucket

#### 📚 Resources

- [AWS S3 sync command documentation](https://awscli.amazonaws.com/v2/documentation/api/latest/reference/s3/sync.html)
- [Pipeline after this lecture](docs/pipeline-configs/lesson-3-08-.gitlab-ci.yml)

### Lesson 9 - Hosting a website on S3

- files in the S3 bucket are not publicly available
- to get the website to work, we need to configure the bucket
- from the bucket, click on *Properties* and enable Static website hosting
- from the bucket, click on the *Permissions* tab and disable *Block public access*
- from the bucket, click on the *Permissions* tab and set a bucket policy

#### 📚 Resources

- [S3 bucket policy example](docs/s3-bucket-policy-example.json)

### Lesson 10 - Controlling when jobs run

- we don’t want to deploy to production from every branch
- to decide which jobs to run, we can use `rules:` to set a condition
- `CI_COMMIT_REF_NAME` gives us the current branch that is running the pipeline
- `CI_DEFAULT_BRANCH` gives us the name of the default branch (typically main or master)

#### 📚 Resources

- [GitLab reference for the .gitlab-ci.yml file - rules:](https://docs.gitlab.com/ee/ci/yaml/#rules)
- [Predefined variables in GitLab](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)
- [Pipeline after this lecture](docs/pipeline-configs/lesson-3-10-.gitlab-ci.yml)

### Lesson 11 - Post-deployment testing

- we will use `cURL` to download the index.html file from the website
- with `grep`, we will check to see if the index.html file contains a specific string

#### 📚 Resources

- [Pipeline after this lecture](docs/pipeline-configs/lesson-3-11-.gitlab-ci.yml)

### Lesson 12 - What is CI/CD?

- the pipeline goes through multiple stages: build, test & deploy
- right now, we consider the website hosted at AWS S3 our production environment
- quite often, pipelines will also have a staging environment
- a staging environment is a non-production, usually non-public environment that is very close to the actual production environment
- we often use automation to create these environments and to ensure that they are indeed identical
- we use a staging environment as a pre-production environment
- essentially, we try out our deployment in the pre-production environment
- CD can refer to two concepts:
  - Continuous Deployment - where every change is automatically deployed to production
  - Continuous Delivery - where every change is automatically deployed to staging but where we need some manual intervention to deploy to production

### Lesson 13 - Assignment

- create a staging environment and add it to the CI/CD pipeline

### Lesson 14 - Assignment solution

#### 📚 Resources

- [Pipeline after this lecture](docs/pipeline-configs/lesson-3-14-.gitlab-ci.yml)

### Lesson 15 - Environments

- every system where we deploy an application is an environment
- typical environments include test, staging & production
- GitLab offers support for environments
- we can define environments in *Deployments > Environments*

#### 📚 Resources

- [Predefined variables in GitLab](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)
- [Pipeline after this lecture](docs/pipeline-configs/lesson-3-15-.gitlab-ci.yml)

### Lesson 16 - Reusing configuration

- sometimes, multiple jobs may look almost the same, and we should try to avoid repeating ourselves
- GitLab allows us to inherit from an exiting job with the `extends:` keyword

#### 📚 Resources

- [Pipeline after this lecture](docs/pipeline-configs/lesson-3-16-.gitlab-ci.yml)

### Lesson 17 - Assignment

- the goal of this assignment is to expand the post-deployment tests to ensure that the correct version has been deployed
- add a file named `version.html` which contains the current build number
- the current build number is given by a predefined GitLab CI variable named `CI_PIPELINE_IID`

#### 📚 Resources

- [Predefined variables in GitLab](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)

### Lesson 18 - Assignment solution

#### 📚 Resources

- [Pipeline after this lecture](docs/pipeline-configs/lesson-3-18-.gitlab-ci.yml)

### Lesson 19 - Continuous Delivery pipeline

- adding `when: manual` allows us to manually trigger the production deployment

#### 📚 Resources

- [Pipeline after this lecture](docs/pipeline-configs/lesson-3-19-.gitlab-ci.yml)

## Unit 4 - Deploying a dockerized application to AWS

### Lesson 1 - Section overview

- modern applications tend to be more complex, and most of them use Docker
- we will build & deploy an application that runs in a Docker container

### Lesson 2 - Introduction to AWS Elastic Beanstalk

- AWS Elastic Beanstalk (AWS EB) is a service that allows us to deploy an application in the AWS cloud without having to worry about managing the virtual server(s) that runs it

### Lesson 3 - Creating a new AWS Elastic Beanstalk application

- when creating an EB app, choose the *Docker* platform and deploy the sample app
- AWS EB will use two AWS services to run the application:
  - EC2 (virtual servers)
  - S3 (storage)
- to deploy a new version, go to the environment in EB and upload the file in templates called `Dockerrun.aws.public.json`

#### 📚 Resources

- [Dockerrun.aws.public.json](templates/Dockerrun.aws.public.json)

### Lesson 4 - Creating the Dockerfile

- create a new file called `Dockerfile` in the root of the project
- add the following contents to it:

```
FROM nginx:1.20-alpine
COPY build /usr/share/nginx/html
```

#### 📚 Resources

- [Official build of Nginx on Dockerhub](https://hub.docker.com/_/nginx)

### Lesson 5 - Building the Docker image

- to build the Docker image, we will use the command `docker build`
- to build Docker images from a GitLab CI pipeline, we need to start the Docker Daemon as a service

#### 📚 Resources

- [docker build command reference](https://docs.docker.com/engine/reference/commandline/build/)
- [Docker in Docker (dind) on Dockerhub](https://hub.docker.com/_/docker)
- [Predefined variables in GitLab](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)
- [docker image ls](https://docs.docker.com/engine/reference/commandline/image_ls/)
- [Pipeline after this lecture](docs/pipeline-configs/lesson-4-05-.gitlab-ci.yml)

### Lesson 6 - Docker container registry

- to preserve a Docker image, we need to push it to a registry
- Dockerhub is a public registry with Docker images
- GitLab offers a private Docker registry which is ideal for private projects

#### 📚 Resources

- [docker login command reference](https://docs.docker.com/engine/reference/commandline/login/)
- [docker push command reference](https://docs.docker.com/engine/reference/commandline/push/)
- [Pipeline after this lecture](docs/pipeline-configs/lesson-4-06-.gitlab-ci.yml)

### Lesson 7 - Testing the container

- we want to test the container to see if the application running on it (web server) is working properly
- to start the container, we will use the `services:` keyword

#### 📚 Resources

- [Pipeline after this lecture](docs/pipeline-configs/lesson-4-07-.gitlab-ci.yml)

### Lesson 8 - Private registry authentication

- AWS EB requires authentication credentials to pull our Docker image
- GitLab allows us to create a Deploy Token that AWS can use to log to the registry
- to generate a Deploy Token, from your project, go to *Settings > Repository > Deploy tokens*.
- create a new Deploy Token with the following scopes:
  - read_repository
  - read_registry

#### 📚 Resources

- [Pipeline after this lecture](docs/pipeline-configs/lesson-4-08-.gitlab-ci.yml)

### Lesson 9 - Deploying to AWS Elastic Beanstalk

- a new deployment to AWS EB happens in two steps
- step 1: we create a new application version with `aws elasticbeanstalk create-application-version`
- step 2: we update the environment with the new application version with `aws elasticbeanstalk update-environment`

#### 📚 Resources

- [Pipeline after this lecture](docs/pipeline-configs/lesson-4-09-.gitlab-ci.yml)

### Lesson 10 - Post-deployment testing

- updating an EB environment does not happen instantly
- we will use `aws elasticbeanstalk wait` to know when the environment has been updated

#### 📚 Resources

- [Pipeline after this lecture](docs/pipeline-configs/lesson-4-10-.gitlab-ci.yml)

## Unit 5 - Conclusion

### Lesson 1 - Final assignment

- request access as a member to the project
- add your name to the list of people who have completed this course

### Lesson 2 - Conclusion

- THANK YOU for sticking until the end

#### 💬 Let's stay in touch

- [Let's connect on LinkedIn](https://www.linkedin.com/in/bolanle-longe/)

#### 📚 Offical GitLab resources

- [10 tips to make you a productive GitLab user](https://about.gitlab.com/blog/2021/02/18/improve-your-gitlab-productivity-with-these-10-tips/)
- [Here's how to do GitOps with GitLab](https://about.gitlab.com/blog/2021/10/21/gitops-with-gitlab/)
- [GitLab Infrastructure management](https://docs.gitlab.com/ee/user/infrastructure/)
